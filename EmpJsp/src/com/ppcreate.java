package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ppcreate
 */
public class ppcreate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ppcreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String name = request.getParameter("name");
		
		String ifsc = request.getParameter("ifsc");
		String phone = request.getParameter("phone");
		String amount = request.getParameter("balance");
		
		try {
		Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/mysql", "root", "root");
        java.sql.Statement stmt=con.createStatement();
        
        ResultSet exists= stmt.executeQuery("select * from ppaccount where phone='"+phone+"'");
        
        if(exists.next()) {
        	out.println("<h2 style = color:red>Phone number already exist... please use another Phone number</h2>");
    		RequestDispatcher rd = request.getRequestDispatcher("ppcreate.jsp");
            rd.include(request, response);
        }
        PreparedStatement pstmt = con.prepareStatement("insert into ppaccount values(?,?,?,?,?)");
        
        
        pstmt.setString(1, name);
        pstmt.setString(2, null);
        pstmt.setString(3, ifsc);
        pstmt.setString(4,phone);
        pstmt.setString(5, amount);
        pstmt.execute();
        pstmt.close();
        con.close();
        out.print("<html><body><b font-color:green;>Account created successfully</b>");
        RequestDispatcher rd = request.getRequestDispatcher("ppcreate.jsp");
        rd.include(request, response);
		} catch (Exception e) {
            System.out.println(e);
        }
	}

}
