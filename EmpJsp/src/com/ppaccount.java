package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ppaccount
 */
public class ppaccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ppaccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String account = request.getParameter("acn");
		String ifsc = request.getParameter("ifsc");
		String balance = request.getParameter("balance");
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection(
	                "jdbc:mysql://localhost:3306/mysql", "root", "root");
	        
            java.sql.Statement stmt=con.createStatement();
	        
	        ResultSet exists= stmt.executeQuery("select * from ppaccount where accountnumber='"+account+"' and ifsc='"+ifsc+"'");
	        
	      
	        if(exists.next()) {
	        System.out.println("enterd");
            
            PreparedStatement pstmt = con.prepareStatement("update ppaccount set balance= balance+? where accountnumber=? and ifsc=?;");
            pstmt.setString(1,balance);
            pstmt.setString(2,account);
            pstmt.setString(3,ifsc);
            pstmt.execute(); 
            pstmt.close();
	        con.close();
	        out.println("<h2 style = color:green>Amount Sent...</h2>");
            RequestDispatcher rd = request.getRequestDispatcher("ppaccount.jsp"); 
            rd.include(request, response);
                
             }else{
                    out.println("<h2 style = color:red>Account number and ifsc not matched try again...</h2>");
                    RequestDispatcher rd = request.getRequestDispatcher("ppaccount.jsp");
                    rd.include(request, response);
            }
            
        }catch (Exception e){
            System.out.println(e);
        }
	}

}
