package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class delete
 */
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String del = request.getParameter("delete");
		String id="";
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection(
	                "jdbc:mysql://localhost:3306/mysql", "root", "root");
            java.sql.Statement stmt=con.createStatement();
	        
	        ResultSet exists= stmt.executeQuery("select * from jsp where id='"+del+"'");
	      
	        if(exists.next()) {
	        	PreparedStatement pstmt = con.prepareStatement("delete from jsp where id =?");
	        	pstmt.setString(1,del);
                pstmt.execute();
                out.println("<h2 style = color:green>Deleted....</h2>");
                RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
                rd.include(request, response);
	        }
        
            else{
            out.println("<h2 style = color:red>Id not exist Try again....</h2>");
            RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
            rd.include(request, response);
        //response.sendRedirect("login.html");
        }
    

	
	     
        }catch (Exception e){
            System.out.println(e);
        }
	}

}
