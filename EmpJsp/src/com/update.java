package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class update
 */
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String id = request.getParameter("id");
		String salary = request.getParameter("amount");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection(
	                "jdbc:mysql://localhost:3306/mysql", "root", "root");
	        
	        
            java.sql.Statement stmt=con.createStatement();
	        
	        ResultSet exists= stmt.executeQuery("select * from jsp where id='"+id+"'");
	        
	        if(exists.next()) {
            PreparedStatement pstmt = con.prepareStatement("update jsp set salary= salary+? where id=?");
            pstmt.setString(1,salary);
            pstmt.setString(2,id);
            pstmt.execute();
            out.println("<h2 style = color:green>Salary Updated...</h2>");
            RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
            rd.include(request, response);    
            }else{
                    out.println("<h2 style = color:red>Id not exist Try again....</h2>");
                    RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
                    rd.include(request, response);
            }
           
        }catch (Exception e){
            System.out.println(e);
        }
		
	

}
}
