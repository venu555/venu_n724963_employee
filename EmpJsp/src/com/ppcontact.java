package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ppcontact
 */
public class ppcontact extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ppcontact() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String phone = request.getParameter("phone");
		String balance = request.getParameter("balance");
		
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection(
	                "jdbc:mysql://localhost:3306/mysql", "root", "root");
           
java.sql.Statement stmt=con.createStatement();
	        
	        ResultSet exists= stmt.executeQuery("select * from ppaccount where phone='"+phone+"'");
	      
	        if(exists.next()) {
	        
            PreparedStatement pstmt = con.prepareStatement("update ppaccount set balance= balance+? where phone=?");
            
            
            pstmt.setString(1,balance);
            pstmt.setString(2,phone);
          
            pstmt.execute(); 
            pstmt.close();
	        out.println("<h2 style = color:green>Amount sent to contact...</h2>");
            RequestDispatcher rd = request.getRequestDispatcher("ppcontact.jsp");
            rd.include(request, response);

	        }
	        else {
	        	out.println("<h2 style = color:red>Phone number does not exist....</h2>");
              RequestDispatcher rd = request.getRequestDispatcher("ppcontact.jsp");
              rd.include(request, response);
	        }            
        }catch (Exception e){
            System.out.println(e);
        }
	}

}
