package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class create
 */
public class tablecreate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tablecreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String id = request.getParameter("id");
		
		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String sal = request.getParameter("sal");
		String des = request.getParameter("des");
		
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection(
	                "jdbc:mysql://localhost:3306/mysql", "root", "root");
java.sql.Statement stmt=con.createStatement();
	        
	        ResultSet exists= stmt.executeQuery("select * from jsp where id='"+id+"'");
	        
	        if(exists.next()) {
	        	out.println("<h2 style = color:red>ID already exist... please use another ID</h2>");
	    		RequestDispatcher rd = request.getRequestDispatcher("tablecreate.jsp");
	            rd.include(request, response);
	        }else {
	        PreparedStatement pstmt = con.prepareStatement("insert into jsp values(?,?,?,?,?)");
	        
	        
	        pstmt.setString(1, id);
	        pstmt.setString(2, name);
	        pstmt.setString(3, age);
	        pstmt.setString(4, sal);
	        pstmt.setString(5, des);
	        pstmt.execute();
	        pstmt.close();
	        con.close();
	        out.print("<html><body><b style = color:green>Account created successfully</b>");
	        RequestDispatcher rd = request.getRequestDispatcher("tablemain.jsp");
	        rd.forward(request, response);
	        }
			} catch (Exception e) {
	            System.out.println(e);
	        }
	}

}
