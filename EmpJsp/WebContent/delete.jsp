<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type = "text/css" href="style.css">
<style>
a:link {
  text-decoration: none;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="login">
    <h1>Delete</h1>
    <form method="post" action= "delete">
        <input type="text" name="delete" placeholder="Enter ID" required="required" />
        <input type="submit" class="btn btn-primary btn-block btn-large" value="delete">
    </form>
</div>
<div>
<a href='main.jsp'><input type="submit" class="btn btn-primary btn-block btn-large" value="Back to Menu"></a>
</div>
</body>
</html>