<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mobile App Demo</title>
</head>
<body>
<div class="third-frame">
<div class="second-frame">
<div class="frame-scroll-correction"></div>
<iframe class="main-frame" src="ppmain.jsp"></iframe>
<a href='main.jsp'><input type="submit" class="btn btn-primary btn-block btn-large" value="Back to Menu"></a>
</div>
</div>

<style>

*{
margin:0px;
padding:0px;
border:0px;
}
body{
background-color:#ececec;
}
.main-frame{
background-color:rgb(255,255,255);
margin-left:auto;
margin-right:auto;
display:block;
position:relative;
z-index:1;
width: 340px;
height: 550px;
}
.second-frame{
height:100%;
width: 340px;
border:solid 20px #000;
border-radius:40px;
border-bottom:solid 80px #000;
border-top:solid 60px #000;
margin-left:auto;
margin-right:auto;
position:relative;
z-index:2;
}
.third-frame{
height:100%;
background-color:#ccc;
padding:5px;
width: 380px;
border-radius:45px;
margin-left:auto;
margin-right:auto;
margin-top:80px;
}
.frame-scroll-correction{
position:absolute;
height:100%;
width:22px;
background-color:#000;
z-index:99;
right:-20px;
}
.resolution-selecter-deploy em{
float:right;
padding-right:20px;
font-size:19px;
font-style:normal;
}
</style>
</body>
</html>