<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<SCRIPT type="text/javascript">
    function noBack() { window.history.forward(); }
</SCRIPT>
</head>
<body onload="onBack()">

<%
try
{
	Class.forName("com.mysql.jdbc.Driver");
    Connection con = DriverManager.getConnection(
            "jdbc:mysql://localhost:3306/mysql", "root", "root");
	session.setAttribute("name", "");
	session.invalidate();
	
	RequestDispatcher rd = request.getRequestDispatcher("login.html");
    rd.include(request, response);
      
}catch (Exception e) {
}
%>
</body>
</html>